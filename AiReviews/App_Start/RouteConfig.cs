﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AiReviews
{
    public class RouteConfig
    {
        // RouteCollection method (passed as routes) enabes routing of URL requests
        // to Controllers, and handles parameters
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // 0: Default route
            // Handles one controller, action, and ID
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new
                {
                    controller = "Home",
                    action = "Index",
                    id = UrlParameter.Optional
                }
            );

            // =================================================================
            // /ActorFilms/
            // =================================================================

            // 1: Add Actor (Film) route
            // Handles subName and FilmID for ActorFilms/Create/
            routes.MapRoute(
                name: "Add Actor (Film)",
                url: "ActorFilms/Create/{subName}/{FilmId}",
                defaults: new
                {
                    controller = "ActorFilms",
                    action = "Create",
                    FilmId = UrlParameter.Optional
                },
                constraints: new
                {
                    // As long as this subName is give, the above route applies
                    subName = "Film"
                }
            );


            // 2: Add Actor (Actor) route
            // Handles subName and FilmID for ActorFilms/Create/
            routes.MapRoute(
                name: "Add Actor (Actor)",
                url: "ActorFilms/Create/{subName}/{ActorId}",
                defaults: new
                {
                    controller = "ActorFilms",
                    action = "Create",
                    // XXX: Is this supposed to be FilmId???
                    FilmId = UrlParameter.Optional
                },
                constraints: new
                {
                    // As long as this subName is give, the above route applies
                    subName = "Actor"
                }
            );

            // =================================================================
            // /DirectorFilms/
            // =================================================================

            // 3: Add Director (Film) route
            // Handles subName and FilmID for DirectorFilms/Create/
            routes.MapRoute(
                name: "Add Director (Film)",
                url: "DirectorFilms/Create/{subName}/{FilmId}",
                defaults: new
                {
                    controller = "DirectorFilms",
                    action = "Create",
                    FilmId = UrlParameter.Optional
                },
                constraints: new
                {
                    // As long as this subName is give, the above route applies
                    subName = "Film"
                }
            );


            // 4: Add Director (Director) route
            // Handles subName and FilmID for DirectorFilms/Create/
            routes.MapRoute(
                name: "Add Director (Director)",
                url: "DirectorFilms/Create/{subName}/{DirectorId}",
                defaults: new
                {
                    controller = "DirectorFilms",
                    action = "Create",
                    // XXX: Is this supposed to be FilmId???
                    FilmId = UrlParameter.Optional
                },
                constraints: new
                {
                    // As long as this subName is give, the above route applies
                    subName = "Director"
                }
            );



        }
    }
}
