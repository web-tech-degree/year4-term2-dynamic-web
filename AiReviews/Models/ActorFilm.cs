﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AiReviews.Models
{


	// Map the model to the database; data annotation
	[Table("ACTOR_FILM")]


    public class ActorFilm
    {


		[Column("actor_film_id")]
		public int ActorFilmId
		{
			get;
			set;
		}


		[Required]
		[Display(Name = "Actor Selection")]
		[Column("actor_id")]
		public int ActorId
		{
			get;
			set;
		}


		[Required]
		[Display(Name = "Film Selection")]
		[Column("film_id")]
		public int FilmId
		{
			get;
			set;
		}
    }
}