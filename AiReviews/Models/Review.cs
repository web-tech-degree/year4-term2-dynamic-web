﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AiReviews.Models
{


	// Map the model to the database; data annotation
	[Table("REVIEW")]


	public class Review
	{


		[Column("review_id")]
		public int ReviewId
		{
			get;
			set;
		}

        [Required]
		[Column("film_id")]
		public int FilmId
		{
			get;
			set;
		}


		// Makes this field required when inputting
		[Required]
		// Column to grab data from
		[Column("review_reviewerName")]
		// Changes display name
		[Display(Name = "Username")]
		public string ReviewReviewerName
		{
			get;
			set;
		}


		[Required]
		[Column("review_content")]
		[Display(Name = "Review Content")]
        // Make the input a textarea
        [DataType(DataType.MultilineText)]
		public string ReviewContent
		{
			get;
			set;
		}

		[Column("review_stars")]
		[Display(Name = "Stars")]
		public int ReviewStars
		{
			get;
			set;
		}

		public string ReviewContentTrimmed
		{
			get
			{
				// Returns either the entire ReviewContent in full if under 100 chars, or returns the first 100 chars with "..." affixed to the end
				if ((ReviewContent.Length) > 100)
				{
					return ReviewContent.Substring(0, 100) + "...";
				}
				else
				{
					return ReviewContent;
				}
			}
		}
	}
}