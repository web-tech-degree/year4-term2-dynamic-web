﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

// See Models\Film..cs for more comments; this code is self-documenting
namespace AiReviews.Models
{


	// Map the model to the database; data annotation
	[Table("ACTOR")]


    public class Actor
    {


        [Column("actor_id")]
        public int ActorId
        {
            get;
            set;
        }


        [Required]
        [Display(Name = "First Name")]
        [Column("actor_fname")]
        public string ActorFname
        {
            get;
            set;
        }


        [Required]
        [Display(Name = "Surname")]
        [Column("actor_sname")]
        public string ActorSname
        {
            get;
            set;
        }


        [Required]
        [Display(Name = "Bio")]
        [DataType(DataType.MultilineText)]
        [Column("actor_desc")]
        public string ActorDesc
        {
            get;
            set;
        }


		// Maps only the date, rather than the date and time
		[DataType(DataType.Date)]
		[DisplayFormat(
			// Sets the display format to use the "Typical UK Format"
			// ... even though ISO 8601 date is superior :3
			DataFormatString = "{0:dd/MM/yyyy}",
			// Provides extra UI for picking date etc. while editing
			ApplyFormatInEditMode = true
			)]
		[Column("actor_dob")]
		[Display(Name = "Birth date")]
		// DateTime? ... the ? makes it a NULLable field
		public DateTime? ActorDateOfBirth
		{
			get;
			set;
		}


        [Column("actor_image")]
        [Display(Name = "Profile Image")]
        public string ActorImage
        {
            get;
            set;
        }


        public string ActorDescTrimmed
        {
            get
            {
                if ((ActorDesc.Length) > 100)
                {
                    return ActorDesc.Substring(0, 100) + "...";
                }
                else
                {
                    return ActorDesc;
                }
            }
        }
    }
}