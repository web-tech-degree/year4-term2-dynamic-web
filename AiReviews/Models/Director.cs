﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

// See Models\Film..cs for more comments; this code is self-documenting
namespace AiReviews.Models
{


	// Map the model to the database; data annotation
	[Table("DIRECTOR")]


	public class Director
	{


		[Column("director_id")]
		public int DirectorId
		{
			get;
			set;
		}


		[Required]
		[Column("director_fname")]
		[Display(Name = "First Name")]
		public string DirectorFname
		{
			get;
			set;
		}


		[Required]
		[Display(Name = "Surname")]
		[Column("director_sname")]
		public string DirectorSname
		{
			get;
			set;
		}


		[Required]
		[Display(Name = "Bio")]
		[DataType(DataType.MultilineText)]
		[Column("director_desc")]
		public string DirectorDesc
		{
			get;
			set;
		}


		// Maps only the date, rather than the date and time
		[DataType(DataType.Date)]
		[DisplayFormat(
			// Sets the display format to use the "Typical UK Format"
			// ... even though ISO 8601 date is superior :3
			DataFormatString = "{0:dd/MM/yyyy}",
			// Provides extra UI for picking date etc. while editing
			ApplyFormatInEditMode = true
			)]
		[Column("director_dob")]
		[Display(Name = "Birth date")]
		// DateTime? ... the ? makes it a NULLable field
		public DateTime? DirectorDateOfBirth
		{
			get;
			set;
		}


		[Column("director_image")]
		[Display(Name = "Profile Image")]
		public string DirectorImage
		{
			get;
			set;
		}


		public string DirectorDescTrimmed
		{
			get
			{
				if ((DirectorDesc.Length) > 100)
				{
					return DirectorDesc.Substring(0, 100) + "...";
				}
				else
				{
					return DirectorDesc;
				}
			}
		}
	}
}