﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AiReviews.Models
{
    public class DBContext : DbContext
    {
        // Bridge between the DB and the models
        // Worksheet 1; Page 29
        public DbSet<Film> Films { get; set; }

        public DbSet<Actor> Actors { get; set; }

        public DbSet<Director> Directors { get; set; }

        public DbSet<ActorFilm> ActorFilms { get; set; }

        public DbSet<DirectorFilm> DirectorFilms { get; set; }

        public DbSet<Review> Reviews { get; set; }

        public DbSet<Genre> Genres { get; set; }


        // XXX: Default bullshit that was made... and I don't know why
        // public System.Data.Entity.DbSet<AiReviews.Models.DirectorFilm> DirectorFilms { get; set; }

        // public System.Data.Entity.DbSet<AiReviews.Models.Director> Directors { get; set; }
    }
}