﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AiReviews.Models
{


	// Map the model to the database; data annotation
	[Table("DIRECTOR_FILM")]


	public class DirectorFilm
	{


		[Column("director_film_id")]
		public int DirectorFilmId
		{
			get;
			set;
		}


		[Required]
		[Display(Name = "Director Selection")]
		[Column("director_id")]
		public int DirectorId
		{
			get;
			set;
		}


		[Required]
		[Display(Name = "Film Selection")]
		[Column("film_id")]
		public int FilmId
		{
			get;
			set;
		}
	}
}