﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AiReviews.Models
{


    // Map the model to the database; data annotation
    [Table ("FILM")]


    public class Film
    {


        [Column ("film_id")]
        // XXX: FilmId should be FilmId
        // Fix this throughout
        public int FilmId
        {
            get;
            set;
        }


        // Makes this field required when inputting
        [Required]
        // Column to grab data from
        [Column ("film_name")]
        // Changes display name
        [Display (Name = "Film Name")]
        public string FilmName
        {
            get;
            set;
        }


        [Required]
        [Column ("film_genre")]
        [Display (Name = "Genre")]
        public string FilmGenre
        {
            get;
            set;
        }


        [Required]
        [Column ("film_desc")]
        // Makes the input field span multiple lines using a textarea
        [DataType(DataType.MultilineText)]
        [Display (Name = "Description")]
        public string FilmDesc
        {
            get;
            set;
        }


        // Maps only the date, rather than the date and time
        [DataType(DataType.Date)]
        [DisplayFormat(
            // Sets the display format to use the "Typical UK Format"
            // ... even though ISO 8601 date is superior :3
            DataFormatString = "{0:dd/MM/yyyy}",
            // Provides extra UI for picking date etc. while editing
            ApplyFormatInEditMode = true
            )]
        [Column ("film_releaseDate")]
        [Display (Name = "Release Date")]
        // DateTime? ... the ? makes it a NULLable field
        public DateTime? FilmReleaseDate
        {
            get;
            set;
        }


        [Column ("film_icon")]
        [Display (Name = "Image")]
        public string FilmIcon
        {
            get;
            set;
        }

        public string FilmDescTrimmed
        {
            get
            {
                // Returns either the entire film description if under 100 chars, or returns the first 100 chars with "..." affixed to the end
                if ((FilmDesc.Length) > 100)
                {
                    return FilmDesc.Substring(0, 100) + "...";
                } else
                {
                    return FilmDesc;
                }
            }
        }
    }
}