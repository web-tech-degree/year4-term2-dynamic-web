﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

// See Models\Film..cs for more comments; this code is self-documenting
namespace AiReviews.Models
{


	// Map the model to the database; data annotation
	[Table("GENRE")]


	public class Genre
	{


		[Column("genre_id")]
		public int GenreId
		{
			get;
			set;
		}


		[Required]
		[Column("genre_name")]
		[Display(Name = "Genre Name")]
		public string GenreName
		{
			get;
			set;
		}


	}
}