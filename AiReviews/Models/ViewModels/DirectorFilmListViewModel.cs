﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AiReviews.Models.ViewModels
{

    // Gets extra details from the Directors and Films for the DirectorFilm
    public class DirectorFilmListViewModel
    {

        //  The DirectorFilm model represented here
        public DirectorFilm DirectorFilmCredit;

        // Link Director to access their data
        public Director Director;

        // Link film to access its data
        public Film Film;
    }
}