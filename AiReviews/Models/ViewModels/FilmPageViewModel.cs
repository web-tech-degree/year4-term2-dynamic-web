﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AiReviews.Models.ViewModels
{
    public class FilmPageViewModel
    {
        // Create Film record
        public Film Film;

        // Related review records
        public IList<Review> Reviews;

        // Related actor records linked via ActorFilms
        public IList<Actor> Actors;

        // Todo: Add Directors
        public IList<Director> Directors;
    }
}