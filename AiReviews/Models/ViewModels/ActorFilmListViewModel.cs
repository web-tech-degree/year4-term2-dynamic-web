﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AiReviews.Models.ViewModels
{

    // Gets extra details from the Actors and Films for the ActorFilm
    public class ActorFilmListViewModel
    {

        //  The ActorFilm model represented here
        public ActorFilm ActorFilmCredit;

        // Link Actor to access their data
        public Actor Actor;

        // Link film to access its data
        public Film Film;
    }
}