// Accepts (and requires) parameter for where it wants to search
const ajaxAutoCompleteSearch = (pLocation, pSearchType) => {
    // This is fucking disgusting, I love it
    // Concatenate the search type to allow different element names to be passed in
    $("[name='" + pSearchType + "']").autocomplete({
        // Define the server result in a higher scope so that its state
        // can be changed, and then accessed on the select function
        resultArray: 0,
        // Set the source of the autocomplete to be the return of
        // this function:
        source: (request, response) => {
            console.log(pLocation);
            $.ajax({
                type: 'GET',
                // Sets the URL to the location where the Search Action points
                // This line was the inline
                // url: '@Url.Action("Search")',
                // This uses the passed parameter
                url: `${pLocation}/Search`,
                // Gets the searchTerm by getting the value of the TextBox
                // Also pass what it is we want to search
                data: {
                    searchFor: pSearchType,
                    searchTerm: $("#" + pSearchType).val()
                },
                // Once the AJAX request succeeds
                success: (serverResult) => {
                    // Pass the result to the higher-scoped variable
                    resultArray = serverResult;
                    // Set the response (what source wants)
                    // to the result of mapping this function over
                    // every object in the serverResult array response
                    // pIndex = index parameter
                    // pItem  = item parameter
                    response($.map(serverResult, (pIndex, pItem) => {
                        // Maps the name and id of every serverResult object
                        // to the variable, using it for the source and
                        // for the select functions
                        resultArray.name = pIndex.name;
                        resultArray.id = pIndex.id;
                        // for the source statement to use
                        return {
                            label: resultArray.name,
                            value: resultArray.id
                        }
                    }));
                },
            });
        },
        // Search when mininmum characters have been input
        minLength: 2,
        // When options selected
        select: (event, ui) => {
            // Explicitly handle specific searches
            // XXX: At this point I should've just wrote new functions, but eh
            if (pSearchType === 'searchFilmGenre' && pLocation === '/Reviews') {
                window.location = '../Films' + '?searchFilmGenre=' + resultArray.name;
            }
            else if (pSearchType === 'searchFilmName' && pLocation === '/Reviews') {
                window.location = '../Films' + '/Details/' + resultArray.id;
            }
            else if (pSearchType === 'searchFilmGenre') {
                window.location = pLocation + '?searchFilmGenre=' + resultArray.name;
            }
            else if (pSearchType === 'searchReviewerName') {
                window.location = pLocation + '?searchReviewerName=' + resultArray.name;
            }
            else {
                // Go to the Details action and append the id
                // from the cleaned up resultArray

                window.location = pLocation + '/Details/' + resultArray.id;

            }
        },
    })
}