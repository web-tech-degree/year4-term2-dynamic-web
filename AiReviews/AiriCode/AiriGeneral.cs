﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AiReviews.AiriCode
{
    public static class AiriGeneral
    {
        // Method to return the reverse of a string
        public static string ReverseString(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}