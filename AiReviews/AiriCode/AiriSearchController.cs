﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AiReviews.Models;
using AiReviews.Models.ViewModels;

namespace AiReviews.AiriCode
{
    public class AiriSearchController : Controller
    {

        // GET: AiriSearch
        public ActionResult Index()
        {
            return View();
        }

        // Takes the DBContext (Films, Actors, Directors) etc.
        // and the searchTerm we want
        public ActionResult SearchName(DBContext db, string searchTerm, string searchLocation)
        {

            // Based on where we want to search
            // Select all the names in the db
            // only get the ID and name
            // pass them into variables
            var searchables =
                from n in db.Films
                select new
                {
                    id = n.FilmId,
                    name = n.FilmName,
                    genre = n.FilmGenre
                };


            // Now check if the searchTerm matches anything in the films var
            searchables = searchables.Where(n =>
                                                n.name.Contains(searchTerm) ||
                                                n.genre.Contains(searchTerm));

            // Convert to JSON; return it
            return Json(searchables, JsonRequestBehavior.AllowGet);
        }
    }
}