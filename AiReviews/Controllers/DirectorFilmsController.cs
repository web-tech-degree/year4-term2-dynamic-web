﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AiReviews.Models;
using AiReviews.Models.ViewModels;

namespace AiReviews.Controllers
{
    public class DirectorFilmsController : Controller
    {
        private DBContext db = new DBContext();

        // GET: DirectorFilms
        public ActionResult Index()
        {
            //  Create a list for the view model to link FIlm and Director
            List<DirectorFilmListViewModel> DirectorFilmList =
                new List<DirectorFilmListViewModel>();

            // Separate list for the DirectorFilm credits to get the keys
            List<DirectorFilm> directorFilmCredits;

            // Populate the DirectorFilms list by selecting all records
            // from the db context
            directorFilmCredits = db.DirectorFilms.ToList();

            // Loop through each record to get the foreign keys
            // then populate the view model with the relevant Film/Director
            // XXX: I think if you make a mess out of director_film table then this shit will fuck up, 
            // not my circus, not my monkeys
            foreach (DirectorFilm i in directorFilmCredits)
            {
                // Match the ID between DirectorFilm and Film, store the result in film
                Film film = db.Films.Where(z => z.FilmId == i.FilmId).Single();

                // Match the ID between DirectorFilm and Film, store the result in Director
                Director director = db.Directors.Where(z => z.DirectorId == i.DirectorId).Single();

                // New DirectorFilmListViewModel object to then add to the list
                DirectorFilmListViewModel toAdd = new DirectorFilmListViewModel();

                // Get the DirectorFilmCredit, Film and Director records.
                // use these to prepare what should be added
                toAdd.DirectorFilmCredit = i;
                toAdd.Film = film;
                toAdd.Director = director;

                // Add to the DirectorFilmList (list of ViewModel objects)
                DirectorFilmList.Add(toAdd);
            }
            // Send the DirectorFilmListViewModel List to the View for dispaly
            return View(DirectorFilmList);
        }

        // GET: DirectorFilms/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectorFilm directorFilm = db.DirectorFilms.Find(id);
            if (directorFilm == null)
            {
                return HttpNotFound();
            }
            return View(directorFilm);
        }

        // GET: DirectorFilms/Create
        public ActionResult Create(int FilmId = 0, int DirectorId = 0)
        {
            // =================================================================
            // FILMS
            // =================================================================
            // From the Films model DbSet
            // select all columns from the database
            // order by the film name
            // LINQ Query
            var filmQuery =
                // Select a model/record type from the DB, use m as keyword
                from m in db.Films
                    // Alphabetical ordering
                orderby m.FilmName
                // Selects which column/fields to return (m is whole model)
                select m;

            // If no id set:
            //      Construct full films dropdown list without preselection
            //      Do so from the query results and display the FIlmname
            //      Store in FilmId in the ViewBag
            // Else
            //      Construct the same way, but with FilmId preselected
            if (FilmId == 0)
            {
                // Collection to send values for the View to access
                ViewBag.FilmId =
                    // Assign a dropdown list value, generated from filmQuery
                    // with value FilmId and name FilmName
                    // null determines no preselection
                    // XXX: The worksheet capitalized the ID here... idk why or if to
                    new SelectList(filmQuery, "FilmId", "FilmName", null);
            }
            else
            {
                ViewBag.FilmId =
                    // FilmId determines preselection: if it was sent to this
                    // method, then we know we should interpret it and display
                    // that
                    // XXX: The worksheet capitalized the ID here... idk why or if to
                    new SelectList(filmQuery, "FilmId", "FilmName", FilmId);
            }

            // =================================================================
            // DirectorS
            // =================================================================
            // From the Directors model DbSet
            // select the fname and sname as a new field called namespace Name
            // and order the Directorid by the sname
            // LINQ Query
            var directorsQuery =
                from a in db.Directors
                orderby a.DirectorSname
                // Create new variable called Name; concatenate it as:
                // Director First Name + *space* + Director Surname
                // and select the ID after the comma
                select new
                {
                    Name = a.DirectorFname + " " + a.DirectorSname,
                    a.DirectorId
                };

            // If no id set:
            //      Construct full films dropdown list without preselection
            //      Do so from the query results and display the concactenated Name
            //      Store in FilmId in the ViewBag
            // Else
            //      Construct the same way, but with DirectorId preselected
            if (DirectorId == 0)
            {
                ViewBag.DirectorId =
                    new SelectList(directorsQuery, "DirectorId", "Name", null);
            }
            else
            {
                ViewBag.DirectorId =
                    new SelectList(directorsQuery, "DirectorId", "Name", DirectorId);
            }

            // Generate the view
            return View();
        }

        // POST: DirectorFilms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DirectorFilmId,DirectorId,FilmId")] DirectorFilm directorFilm)
        {
            if (ModelState.IsValid)
            {
                db.DirectorFilms.Add(directorFilm);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(directorFilm);
        }

        // GET: DirectorFilms/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectorFilm directorFilm = db.DirectorFilms.Find(id);
            if (directorFilm == null)
            {
                return HttpNotFound();
            }

            // =================================================================
            // FILMS
            // =================================================================
            // From the Films model DbSet
            // select all columns from the database
            // order by the film name
            // LINQ Query
            var filmQuery =
                from m in db.Films
                orderby m.FilmName
                select m;
            // Construct full films dropdown list, preselected with foreign key
            // Do so from the query results and display the FIlmname
            // Store in FilmId in the ViewBag
            // XXX: The worksheet capitalized the ID here... idk why or if to
            ViewBag.FilmId = new SelectList(filmQuery, "FilmId", "FilmName", directorFilm.FilmId);

            // =================================================================
            // DirectorS
            // =================================================================
            // From the Directors model DbSet
            // select the fname and sname as a new field called namespace Name
            // and order the Directorid by the sname
            // LINQ Query
            var DirectorQuery =
                from a in db.Directors
                orderby a.DirectorSname
                select new
                {
                    Name = a.DirectorFname + " " + a.DirectorSname,
                    a.DirectorId
                };

            // Construct full films dropdown list, preselected with foreign key
            // Do so from the query results and display the concactenated Name
            // Store in FilmId in the ViewBag
            ViewBag.DirectorId = new SelectList(DirectorQuery, "DirectorId", "Name", directorFilm.DirectorId);

            return View(directorFilm);
        }

        // POST: DirectorFilms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DirectorFilmId,DirectorId,FilmId")] DirectorFilm directorFilm)
        {
            if (ModelState.IsValid)
            {
                db.Entry(directorFilm).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(directorFilm);
        }

        // GET: DirectorFilms/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DirectorFilm directorFilm = db.DirectorFilms.Find(id);
            if (directorFilm == null)
            {
                return HttpNotFound();
            }
            return View(directorFilm);
        }

        // POST: DirectorFilms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DirectorFilm directorFilm = db.DirectorFilms.Find(id);
            db.DirectorFilms.Remove(directorFilm);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}