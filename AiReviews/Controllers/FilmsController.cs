﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AiReviews.Models;
using AiReviews.Models.ViewModels;
using PagedList;
// using static AiReviews.AiriCode.AiriGeneral;

namespace AiReviews.Controllers
{
    public class FilmsController : Controller
    {
        private DBContext db = new DBContext();

        // JSON/AJAX Autocomplete on search using autocomplete jQuery UI plugin
        public ActionResult Search(string searchFor, string searchTerm)
        {

            // Depending on what user is "searching for", select only matching items based on that
            if (searchFor == "searchFilmName")
            {
                // Select all the films
                var searchables = from f in db.Films
                                  select new
                                  {
                                      id = f.FilmId,
                                      name = f.FilmName,
                                  };
                searchables = searchables.Where(n => n.name.Contains(searchTerm));

                return Json(searchables, JsonRequestBehavior.AllowGet);
            }

            if (searchFor == "searchFilmGenre")
            {
                // Select all the genres
                var searchables = from f in db.Genres
                                  select new
                                  {
                                      id = f.GenreId,
                                      name = f.GenreName
                                  };
                searchables = searchables.Where(n => n.name.Contains(searchTerm));

                return Json(searchables, JsonRequestBehavior.AllowGet);
            }
            return View();

        }

        // GET: Films
        public ActionResult Index(int? page, string lastSort, string clickedSort, string currentFilterFilmName, string searchFilmName, string currentFilterFilmGenre, string searchFilmGenre)
        {
            // For the ViewBag to keep a note of current sort order
            if (!String.IsNullOrEmpty(lastSort))
            {
                ViewBag.LastSort = lastSort;
            }
            // Make defualt be firstName
            else
            {
                ViewBag.LastSort = "filmName";
            }

            // If clickedSort contains something, aka an option was clicked
            if (!String.IsNullOrEmpty(clickedSort))
            {
                // If the current sort "contains" the shameclicked sort, aka clicked current option (regardless of if it's inverted)
                if (ViewBag.LastSort.Contains(clickedSort))
                {
                    // If it was already "inverted", trim it to not identify as inverted
                    if (ViewBag.LastSort.Contains("Inverted"))
                    {
                        ViewBag.LastSort = clickedSort;
                    }
                    // Otherwise, suffix "inverted"
                    else
                    {
                        ViewBag.LastSort = lastSort + "Inverted";
                    }
                }
                // Since the current sort didn't "contain" the clicked sort, just make it normally sorted via that type
                else
                {
                    ViewBag.LastSort = clickedSort;
                }
            }

            // Get names from genres DB
            var genreQuery = from g in db.Genres
                             select new
                             {
                                 g.GenreName
                             };

            //
            //
            // Here lays the code I tried to add a first item to the list...
            // "What the fuck Adrian"
            // 
            //
            //

            // Convert this shit to a list of strings
            List<string> genreQueryList = new List<string>();
            foreach (var i in genreQuery)
            {
                genreQueryList.Add(i.GenreName);
            }

            // Add an "All Genres" option in the first position because I'm stubborn and don't want that in the DB
            // ... maybe I should've done just that, though
            genreQueryList.Insert(0, "All Genres");

            // Pass ViewData SelectList so it can be used in DropDownList, make the name match what the searchFilmGenre uses
            var filmGenreSelectlist = new SelectList(genreQueryList);

            ViewData["searchFilmGenre"] = filmGenreSelectlist;

            //
            //
            //
            //
            //

            // Reset to page 1 if user searched, otherwise push the current params into what will be checked next
            if (searchFilmName != null)
                page = 1;
            else
                searchFilmName = currentFilterFilmName;

            if (searchFilmGenre != null)
                page = 1;
            else
                searchFilmGenre = currentFilterFilmGenre;



            var films = from f in db.Films select f;

            string viewBagCurrentFilmGenre = ViewBag.CurrentFilterFilmGenre;
            string viewBagCurrentFilmName = ViewBag.CurrentFilterFilmName;

            // If user just input film name search, filter by last genre then by new name search.
            // If last film genre is "All Genres", don't filter
            if (!String.IsNullOrEmpty(searchFilmName))
            {
                if (currentFilterFilmGenre != "All Genres" && viewBagCurrentFilmGenre != null)
                {
                    films = films.Where(z => z.FilmGenre.Contains(viewBagCurrentFilmGenre));
                }
                films = films.Where(z => z.FilmName.Contains(searchFilmName));
            }

            // If user just input film genre, filter by last name search then by new genre
            // If new film genre is "All Genres", don't filter
            // If last film name is null, don't filter
            if (!String.IsNullOrEmpty(searchFilmGenre))
            {
                if (!String.IsNullOrEmpty(viewBagCurrentFilmName))
                {
                    films = films.Where(z => z.FilmName.Contains(viewBagCurrentFilmName));
                }
                if (searchFilmGenre != "All Genres" && searchFilmGenre != null)
                {
                    films = films.Where(z => z.FilmGenre.Contains(searchFilmGenre));
                }
            }

            // Update ViewBag
            ViewBag.CurrentFilterFilmName = searchFilmName;
            ViewBag.CurrentFilterFilmGenre = searchFilmGenre;

            // Go over what the new sort should be and apply it
            switch (ViewBag.lastSort)
            {
                case "filmName":
                    films = films.OrderBy(z => z.FilmName);
                    break;
                case "filmNameInverted":
                    films = films.OrderByDescending(z => z.FilmName);
                    break;
                case "filmGenre":
                    films = films.OrderBy(z => z.FilmGenre);
                    break;
                case "filmGenreInverted":
                    films = films.OrderByDescending(z => z.FilmGenre);
                    break;
                case "filmReleaseDate":
                    films = films.OrderBy(z => z.FilmReleaseDate);
                    break;
                case "filmReleaseDateInverted":
                    films = films.OrderByDescending(z => z.FilmReleaseDate);
                    break;
                default:
                    films = films.OrderBy(z => z.FilmName);
                    break;
            }

            // How many records per page (could also be a param...)
            int pageSize = 3;

            // If page is null: set to 1
            // otherwise: keep page value
            int pageNumber = (page ?? 1);

            // Send the updated films list to the View: Use PagedList to implement pagination,
            // and proide the pageSize
            return View(films.ToPagedList(pageNumber, pageSize));
        }

        // GET: Films/Details/51
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Film film = db.Films.Find(id);

            if (film == null)
            {
                return HttpNotFound();
            }

            // New view model object
            FilmPageViewModel filmPage = new FilmPageViewModel();

            // Get the current film and assign to view model
            filmPage.Film = film;
            // Populate the Reviews list for the view model by matching all reviews where the film id matches
            filmPage.Reviews = db.Reviews.Where(z => z.FilmId == film.FilmId).ToList();

            // For actors, first we need to get all related records in the join table
            IList<ActorFilm> actorLinks = db.ActorFilms.Where(z => z.FilmId == film.FilmId).ToList();

            IList<DirectorFilm> directorLinks = db.DirectorFilms.Where(z => z.FilmId == film.FilmId).ToList();

            // Then we'll construct a list of the actor/director records to match
            IList<Actor> actors = new List<Actor>();

            IList<Director> directors = new List<Director>();


            // Here we loop through the ActorFilm records in the join table
            // Add the matching actor record to the list for each matching actor
            foreach (ActorFilm i in actorLinks)
                if (!String.IsNullOrEmpty(i.FilmId.ToString()))
                    actors.Add(db.Actors.Where(z => z.ActorId == i.ActorId).Single());

            foreach (DirectorFilm i in directorLinks)
                if (!String.IsNullOrEmpty(i.DirectorId.ToString()))
                    directors.Add(db.Directors.Where(z => z.DirectorId == i.DirectorId).Single());

            // Once populated, we can assign the list of actors/directors to the view model
            filmPage.Actors = actors;
            filmPage.Directors = directors;

            // Here we will use a LINQ query to get the average review score for reviews
            // related to this film
            // Using the null-coalescence operator (??):
            //      check If z.ReviewStars is null, set it to 0, else leave it
            var toBeAverageReview =
                db.Reviews.Where(z => z.FilmId == film.FilmId)
                .Average(z => (double?)z.ReviewStars) ?? 0;

            // XXX: Fuck FontAwesome, it's not displaying stars with decimals
            // FIXME: Don't use FontAwesome.
            ViewBag.AverageReview = Math.Floor(toBeAverageReview);

            // Reutnr the view model to use
            return View(filmPage);
        }

        // GET: Films/Create
        public ActionResult Create()
        {
            // Get names from genres DB
            var genreQuery = from g in db.Genres
                             select new
                             {
                                 g.GenreName
                             };

            // Pass ViewData SelectList so it can be used in DropDownList, both value and text field are the genrename
            ViewData["FilmGenre"] = new SelectList(genreQuery, "GenreName", "GenreName");


            return View();
        }

        // POST: Films/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        // Bind all the fields from the model to a Film instance called film
        // Get the uploaded (posted) file and associates it for handling using the name upload
        public ActionResult Create([Bind(Include = "FilmId,FilmName,FilmGenre,FilmDesc,FilmReleaseDate,FilmImage")] Film film, HttpPostedFileBase upload)
        {
            // If the data in the form is valid
            if (ModelState.IsValid)
            {
                // Check if a file was uploaded
                if (upload != null && upload.ContentLength > 0)
                {
                    // Check MIME types against the valid types below
                    if (upload.ContentType == "image/jpeg" ||
                        upload.ContentType == "image/jpg" ||
                        upload.ContentType == "image/gif" ||
                        upload.ContentType == "image/png")
                    {
                        // Construct a path to put the file in an Images subfolder in Content
                        string path = Path.Combine(
                            Server.MapPath("~/Content/Images"),
                            Path.GetFileName(upload.FileName)
                        );

                        // Save the file to that path location
                        upload.SaveAs(path);

                        // Store the relative path to the image in the database
                        film.FilmIcon = "~/Content/Images/" + Path.GetFileName(upload.FileName);
                    }
                    else
                    {
                        // Construct a message that can be displayed in the view
                        ViewBag.Message = "Not valid image format";
                    }
                }
                // Add the film to the database and save
                db.Films.Add(film);
                db.SaveChanges();
                // Redirect to Index
                return RedirectToAction("Index");
            }
            return View(film);
        }

        // GET: Films/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Film film = db.Films.Find(id);
            if (film == null)
            {
                return HttpNotFound();
            }
            return View(film);
        }

        // POST: Films/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FilmId,FilmName,FilmGenre," + "FilmDesc,FilmReleaseDate,FilmImage")] Film film, HttpPostedFileBase upload)
        {
            // If the data in the form is valid
            if (ModelState.IsValid)
            {
                // Check if a file was uploaded
                if (upload != null && upload.ContentLength > 0)
                {
                    // Check MIME types against the valid types below
                    if (upload.ContentType == "image/jpeg" ||
                        upload.ContentType == "image/jpg" ||
                        upload.ContentType == "image/gif" ||
                        upload.ContentType == "image/png")
                    {
                        // Construct a path to put the file in an Images subfolder in Content
                        string path = Path.Combine(
                            Server.MapPath("~/Content/Images"),
                            Path.GetFileName(upload.FileName)
                        );

                        // Save the file to that path location
                        upload.SaveAs(path);

                        // Store the relative path to the image in the database
                        film.FilmIcon = "~/Content/Images/" + Path.GetFileName(upload.FileName);
                    }
                    else
                    {
                        // Construct a message that can be displayed in the view
                        ViewBag.Message = "Not valid image format";
                    }
                }

                db.Entry(film).State = EntityState.Modified;
                db.SaveChanges();
                // Redirect to Index
                return RedirectToAction("Index");
            }

            return View(film);
        }

        // GET: Films/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Film film = db.Films.Find(id);
            if (film == null)
            {
                return HttpNotFound();
            }
            return View(film);
        }

        // POST: Films/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Film film = db.Films.Find(id);
            db.Films.Remove(film);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}