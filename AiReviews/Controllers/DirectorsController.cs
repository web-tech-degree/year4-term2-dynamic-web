﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AiReviews.Models;
using AiReviews.Models.ViewModels;
using PagedList;

namespace AiReviews.Controllers
{
    public class DirectorsController : Controller
    {
        private DBContext db = new DBContext();
        public ActionResult Search(string searchTerm)
        {
            // Based on where we want to search
            // Select all the names in the db
            // only get the ID and name
            // pass them into variables
            var searchables =
                from n in db.Directors
                select new
                {
                    id = n.DirectorId,
                    name = n.DirectorFname,
                    extra1 = n.DirectorSname,
                };

            // Now check if the searchTerm matches anything in the names var
            searchables = searchables.Where(n =>
               n.name.Contains(searchTerm) ||
               n.extra1.Contains(searchTerm));

            // Convert to JSON; return it
            return Json(searchables, JsonRequestBehavior.AllowGet);
        }

        // GET: Directors
        public ActionResult Index(int? page, string lastSort, string clickedSort, string currentName, string searchName, string currentGenre, string searchGenre)
        {
            // For the ViewBag to keep a note of current sort order
            if (!String.IsNullOrEmpty(lastSort))
            {
                ViewBag.LastSort = lastSort;
            }
            // Make defualt be firstName
            else
            {
                ViewBag.LastSort = "firstName";
            }

            // If clickedSort contains something, aka an option was clicked
            if (!String.IsNullOrEmpty(clickedSort))
            {
                // If the current sort "contains" the clicked sort, aka clicked current option (regardless of if it's inverted)
                if (ViewBag.LastSort.Contains(clickedSort))
                {
                    // If it was already "inverted", trim it to not identify as inverted
                    if (ViewBag.LastSort.Contains("Inverted"))
                    {
                        ViewBag.LastSort = clickedSort;
                    }
                    // Otherwise, suffix "inverted"
                    else
                    {
                        ViewBag.LastSort = lastSort + "Inverted";
                    }
                }
                // Since the current sort didn't "contain" the clicked sort, just make it normally sorted via that type
                else
                {
                    ViewBag.LastSort = clickedSort;
                }
            }

            // Reset to page 1 if user searched, otherwise push the current params into what will be checked next
            if (searchName != null)
                page = 1;
            else
                searchName = currentName;

            // Update ViewBag
            ViewBag.SearchName = searchName;

            // Select all the directors in the db
            var directors = from f in db.Directors select f;

            // Check if the search string is not empty
            // Get only the directors that match the inputted name
            // Concatenate the firstname and surname so the user can search for both together
            if (!String.IsNullOrEmpty(searchName))
            {
                directors = directors.Where(z => (z.DirectorFname + " " + z.DirectorSname).Contains(searchName));
            }

            // Go over what the new sort should be and apply it
            switch (ViewBag.lastSort)
            {
                case "firstName":
                    directors = directors.OrderBy(z => z.DirectorFname);
                    break;
                case "firstNameInverted":
                    directors = directors.OrderByDescending(z => z.DirectorFname);
                    break;
                case "lastName":
                    directors = directors.OrderBy(z => z.DirectorSname);
                    break;
                case "lastNameInverted":
                    directors = directors.OrderByDescending(z => z.DirectorSname);
                    break;
                case "birthDate":
                    directors = directors.OrderBy(z => z.DirectorDateOfBirth);
                    break;
                case "birthDateInverted":
                    directors = directors.OrderByDescending(z => z.DirectorDateOfBirth);
                    break;
                default:
                    directors = directors.OrderBy(z => z.DirectorFname);
                    break;
            }

            // How many records per page (could also be a param...)
            int pageSize = 3;

            // If page is null: set to 1
            // otherwise: keep page value
            int pageNumber = (page ?? 1);

            // Send the updated films list to the View: Use PagedList to implement pagination,
            // and proide the pageSize
            return View(directors.ToPagedList(pageNumber, pageSize));
        }

        // GET: Directors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Director activeDirector= db.Directors.Find(id);
            if (activeDirector == null)
            {
                return HttpNotFound();
            }

            //  Create a list for the view model to link Film and Actor
            List<DirectorFilmListViewModel> DirectorFilmList =
                new List<DirectorFilmListViewModel>();

            // Separate list for the DirectorFilm credits to get the keys
            List<DirectorFilm> directorFilmCredits;

            // Populate the DirectorFilms list by selecting all records
            // from the db context
            directorFilmCredits = db.DirectorFilms.ToList();

            // Keep track of whether or not at least one director film will be spat out
            bool atLeastOneResultFound = false;



            // Loop through each record to get the foreign keys
            // then populate the view model with the relevant Film/Director
            foreach (DirectorFilm i in directorFilmCredits)
            {
                // Only run on the current actor
                if (i.DirectorId == id)
                {
                    atLeastOneResultFound = true;
                    Film film = db.Films.Where(z => z.FilmId == i.FilmId).Single();
                    Director director = db.Directors.Where(z => z.DirectorId == i.DirectorId).Single();
                    DirectorFilmListViewModel toAdd = new DirectorFilmListViewModel();
                    toAdd.DirectorFilmCredit = i;
                    toAdd.Film = film;
                    toAdd.Director = director;
                    DirectorFilmList.Add(toAdd);
                }
            }
            // If at least one actor was found, ViewBag that it was lul I'm pretty sure this is not how I should do it
            if (atLeastOneResultFound)
            {
                ViewBag.DirectorDirectedAtleastOneMovie = true;
            }
            // If no actor films found, add the actor from the actor db into the viewmodel
            else
            {
                ViewBag.DirectorDirectedAtleastOneMovie = false;
                // New ActorFilmListViewModel object to then add to the list
                DirectorFilmListViewModel toAdd = new DirectorFilmListViewModel();
                toAdd.Director = activeDirector;
                DirectorFilmList.Add(toAdd);
            }
            // Send the ActorFilmListViewModel List to the View for dispaly
            return View(DirectorFilmList);
            // return View(activeActor);
        }

        // GET: Directors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Directors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DirectorId,DirectorFname,DirectorSname," + "DirectorDesc,DirectorDateOfBirth,DirectorImage")] Director director, HttpPostedFileBase upload)
        {
            // If the data in the form is valid
            if (ModelState.IsValid)
            {
                // Check if a file was uploaded
                if (upload != null && upload.ContentLength > 0)
                {
                    // Check MIME types against the valid types below
                    if (upload.ContentType == "image/jpeg" ||
                        upload.ContentType == "image/jpg" ||
                        upload.ContentType == "image/gif" ||
                        upload.ContentType == "image/png")
                    {
                        // Construct a path to put the file in an Images subfolder in Content
                        string path = Path.Combine(
                            Server.MapPath("~/Content/Images"),
                            Path.GetFileName(upload.FileName)
                        );

                        // Save the file to that path location
                        upload.SaveAs(path);

                        // Store the relative path to the image in the database
                        director.DirectorImage = "~/Content/Images/" + Path.GetFileName(upload.FileName);
                    }
                    else
                    {
                        // Construct a message that can be displayed in the view
                        ViewBag.Message = "Not valid image format";
                    }
                }
                // Add the actor to the database and save
                db.Directors.Add(director);
                db.SaveChanges();
                // Redirect to Index
                return RedirectToAction("Index");
            }

            return View(director);
        }

        // GET: Directors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Director director = db.Directors.Find(id);
            if (director == null)
            {
                return HttpNotFound();
            }
            return View(director);
        }

        // POST: Directors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DirectorId,DirectorFname,DirectorSname," + "DirectorDesc,DirectorDateOfBirth,DirectorImage")] Director director, HttpPostedFileBase upload)
        {
            // If the data in the form is valid
            if (ModelState.IsValid)
            {
                // Check if a file was uploaded
                if (upload != null && upload.ContentLength > 0)
                {
                    // Check MIME types against the valid types below
                    if (upload.ContentType == "image/jpeg" ||
                        upload.ContentType == "image/jpg" ||
                        upload.ContentType == "image/gif" ||
                        upload.ContentType == "image/png")
                    {
                        // Construct a path to put the file in an Images subfolder in Content
                        string path = Path.Combine(
                            Server.MapPath("~/Content/Images"),
                            Path.GetFileName(upload.FileName)
                        );

                        // Save the file to that path location
                        upload.SaveAs(path);

                        // Store the relative path to the image in the database
                        director.DirectorImage = "~/Content/Images/" + Path.GetFileName(upload.FileName);
                    }
                    else
                    {
                        // Construct a message that can be displayed in the view
                        ViewBag.Message = "Not valid image format";
                    }
                }

                db.Entry(director).State = EntityState.Modified;
                db.SaveChanges();
                // Redirect to Index
                return RedirectToAction("Index");
            }

            return View(director);
        }

        // GET: Directors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Director director = db.Directors.Find(id);
            if (director == null)
            {
                return HttpNotFound();
            }
            return View(director);
        }

        // POST: Directors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Director director = db.Directors.Find(id);
            db.Directors.Remove(director);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}