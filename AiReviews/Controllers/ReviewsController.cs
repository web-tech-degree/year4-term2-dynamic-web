﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AiReviews.Models;
using AiReviews.Models.ViewModels;
using PagedList;

namespace AiReviews.Controllers
{
    public class ReviewsController : Controller
    {
        private DBContext db = new DBContext();
        public ActionResult Search(string searchFor, string searchTerm)
        {
            // Create a list for the view model to link FilmReviews and Films
            List<FilmReviewViewModel> FilmReviewList =
                new List<FilmReviewViewModel>();

            // List of review objects to cycle through and map ids
            List<Review> Reviews;

            // Populate the Reviews list by selecting all records
            // from the db context
            Reviews = db.Reviews.ToList();

            // Loop through each review in the list of data (each row)
            foreach (Review i in Reviews)
            {
                // Match the ID between Review and Films, store the result in film
                Film film = db.Films.Where(z => z.FilmId == i.FilmId).Single();

                // New DirectorFilmListViewModel object to then add to the list
                FilmReviewViewModel toAdd = new FilmReviewViewModel();

                // Get the Review and Film
                // use these to prepare what should be added
                toAdd.Review = i;
                toAdd.Film = film;

                // Add to the FilmReviewList (list of ViewModel objects)
                FilmReviewList.Add(toAdd);
            }
            
            // Depending on what user is "searching for", select only matching items based on that
            if (searchFor == "searchFilmName")
            {
                var searchables = from f in db.Films
                                  select new
                                  {
                                      id = f.FilmId,
                                      name = f.FilmName,
                                  };

                searchables = searchables.Where(n => n.name.Contains(searchTerm));

                return Json(searchables, JsonRequestBehavior.AllowGet);
            }

            if (searchFor == "searchReviewerName")
            { 
                var searchables = from f in FilmReviewList
                                  select new
                                  {
                                      id = f.Review.ReviewId,
                                      name = f.Review.ReviewReviewerName
                                  };

                searchables = searchables.Where(n => n.name.Contains(searchTerm));

                return Json(searchables, JsonRequestBehavior.AllowGet);
            }
            return View();
        }

        // GET: Reviews
        public ActionResult Index(int? page, string lastSort, string clickedSort, string currentFilterFilmName, string searchFilmName, string currentFilterReviewerName, string searchReviewerName)
        {
            // Create a list for the view model to link FilmReviews and Films
            List<FilmReviewViewModel> FilmReviewList =
                new List<FilmReviewViewModel>();

            // List of review objects to cycle through and map ids
            List<Review> Reviews;

            // Populate the Reviews list by selecting all records
            // from the db context
            Reviews = db.Reviews.ToList();

            // Loop through each review in the list of data (each row)
            foreach (Review i in Reviews)
            {
                // Match the ID between Review and Films, store the result in film
                Film film = db.Films.Where(z => z.FilmId == i.FilmId).Single();

                // New DirectorFilmListViewModel object to then add to the list
                FilmReviewViewModel toAdd = new FilmReviewViewModel();

                // Get the Review and Film
                // use these to prepare what should be added
                toAdd.Review = i;
                toAdd.Film = film;

                // Add to the FilmReviewList (list of ViewModel objects)
                FilmReviewList.Add(toAdd);
            }

            // For the ViewBag to keep a note of current sort order
            if (!String.IsNullOrEmpty(lastSort))
            {
                ViewBag.LastSort = lastSort;
            }
            // Make defualt be firstName
            else
            {
                ViewBag.LastSort = "filmName";
            }

            // If clickedSort contains something, aka an option was clicked
            if (!String.IsNullOrEmpty(clickedSort))
            {
                // If the current sort "contains" the clicked sort, aka clicked current option (regardless of if it's inverted)
                if (ViewBag.LastSort.Contains(clickedSort))
                {
                    // If it was already "inverted", trim it to not identify as inverted
                    if (ViewBag.LastSort.Contains("Inverted"))
                    {
                        ViewBag.LastSort = clickedSort;
                    }
                    // Otherwise, suffix "inverted"
                    else
                    {
                        ViewBag.LastSort = lastSort + "Inverted";
                    }
                }
                // Since the current sort didn't "contain" the clicked sort, just make it normally sorted via that type
                else
                {
                    ViewBag.LastSort = clickedSort;
                }
            }

            // Reset to page 1 if user searched, otherwise push the current params into what will be checked next
            if (searchFilmName != null)
                page = 1;
            else
                searchFilmName = currentFilterFilmName;

            if (searchReviewerName != null)
                page = 1;
            else
                searchReviewerName = currentFilterReviewerName;

           

            var filmReviewList = from f in FilmReviewList select f;

            // See films index:
            string viewBagCurrentFilmName = ViewBag.CurrentFilterFilmName;
            string viewBagCurrentReviewerName = ViewBag.CurrentFilterReviewerName;

            if (!String.IsNullOrEmpty(searchFilmName))
            {
                if (viewBagCurrentReviewerName != null)
                    filmReviewList = filmReviewList.Where(z => z.Review.ReviewReviewerName.Contains(viewBagCurrentReviewerName));
                filmReviewList = filmReviewList.Where(z => z.Film.FilmName.Contains(searchFilmName));
            }

            if (!String.IsNullOrEmpty(searchReviewerName))
            {
                if (!String.IsNullOrEmpty(viewBagCurrentFilmName))
                    filmReviewList = filmReviewList.Where(z => z.Film.FilmName.Contains(viewBagCurrentFilmName));
                filmReviewList = filmReviewList.Where(z => z.Review.ReviewReviewerName.Contains(searchReviewerName));
            }

            // Update ViewBag
            ViewBag.CurrentFilterFilmName = searchFilmName;
            ViewBag.CurrentFilterReviewerName = searchReviewerName;

            // Go over what the new sort should be and apply it
            switch (ViewBag.lastSort)
            {
                case "filmName":
                    filmReviewList = filmReviewList.OrderBy(z => z.Film.FilmName);
                    break;
                case "filmNameInverted":
                    filmReviewList = filmReviewList.OrderByDescending(z => z.Film.FilmName);
                    break;
                case "reviewerName":
                    filmReviewList = filmReviewList.OrderBy(z => z.Review.ReviewReviewerName);
                    break;
                case "reviewerNameInverted":
                    filmReviewList = filmReviewList.OrderByDescending(z => z.Review.ReviewReviewerName);
                    break;
                case "reviewRating":
                    filmReviewList = filmReviewList.OrderBy(z => z.Review.ReviewStars);
                    break;
                case "reviewRatingInverted":
                    filmReviewList = filmReviewList.OrderByDescending(z => z.Review.ReviewStars);
                    break;
                default:
                    filmReviewList = filmReviewList.OrderBy(z => z.Film.FilmName);
                    break;
            }

            // How many records per page (could also be a param...)
            int pageSize = 3;

            // If page is null: set to 1
            // otherwise: keep page value
            int pageNumber = (page ?? 1);

            // Send the updated films list to the View: Use PagedList to implement pagination,
            // and proide the pageSize
            return View(filmReviewList.ToPagedList(pageNumber, pageSize));
        }

        // GET: Reviews/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review review = db.Reviews.Find(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            // Find the related film
            Film film = db.Films.Where(z => z.FilmId == review.FilmId).Single();
            // Create a new view model object and assign the review and film details
            FilmReviewViewModel FilmReview = new FilmReviewViewModel();
            FilmReview.Review = review;
            FilmReview.Film = film;

            // Sebd the view model to the View
            return View(FilmReview);
        }

        // GET: Reviews/Create
        public ActionResult Create(int id = 0)
        {
            // If no id sent, redirect to reviews index
            // Makes sure reviews can only be made for existing films
            if (id == 0)
            {
                return RedirectToAction("Index");
            }
            // Otherwise, select the film the id matches
            Film film = db.Films.Where(z => z.FilmId == id).Single();

            // Then populate these values in the ViewBag
            ViewBag.FilmId = id;
            ViewBag.FilmName = film.FilmName;

            return View();
        }

        // POST: Reviews/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ReviewId,FilmId,ReviewReviewerName,ReviewContent,ReviewStars")] Review review)
        {
            if (ModelState.IsValid)
            {
                db.Reviews.Add(review);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(review);
        }

        // GET: Reviews/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review review = db.Reviews.Find(id);
            if (review == null)
            {
                return HttpNotFound();
            }

            // If we have a review, select film where id matches
            Film film = db.Films.Where(z => z.FilmId == review.FilmId).Single();

            // Then populate these values in the ViewBag
            ViewBag.Filmid = film.FilmId;
            ViewBag.FilmName = film.FilmName;

            return View(review);
        }

        // POST: Reviews/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ReviewId,FilmId,ReviewReviewerName,ReviewContent,ReviewStars")] Review review)
        {
            if (ModelState.IsValid)
            {
                db.Entry(review).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(review);
        }

        // GET: Reviews/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review review = db.Reviews.Find(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            return View(review);
        }

        // POST: Reviews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Review review = db.Reviews.Find(id);
            db.Reviews.Remove(review);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}