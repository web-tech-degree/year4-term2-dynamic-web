﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AiReviews.Models;
using AiReviews.Models.ViewModels;

namespace AiReviews.Controllers
{
    public class ActorFilmsController : Controller
    {
        private DBContext db = new DBContext();

        // GET: ActorFilms
        public ActionResult Index()
        {
            //  Create a list for the view model to link FIlm and Actor
            List<ActorFilmListViewModel> ActorFilmList =
                new List<ActorFilmListViewModel>();

            // Separate list for the ActorFilm credits to get the keys
            List<ActorFilm> actorFilmCredits;

            // Populate the ActorFilms list by selecting all records
            // from the db context
            actorFilmCredits = db.ActorFilms.ToList();

            // Loop through each record to get the foreign keys
            // then populate the view model with the relevant Film/Actor
            // XXX: I think if you make a mess out of actor_film table then this shit just dies, 
            // not my circus, not my monkeys
            foreach (ActorFilm i in actorFilmCredits)
            {
                // Match the ID between ActorFilm and Film, store the result in film
                Film film = db.Films.Where(z => z.FilmId == i.FilmId).Single();

                // Match the ID between ActorFilm and Film, store the result in actor
                Actor actor = db.Actors.Where(z => z.ActorId == i.ActorId).Single();

                // New ActorFilmListViewModel object to then add to the list
                ActorFilmListViewModel toAdd = new ActorFilmListViewModel();

                // Get the ActorFilmCredit, Film and Actor records.
                // use these to prepare what should be added
                toAdd.ActorFilmCredit = i;
                toAdd.Film = film;
                toAdd.Actor = actor;

                // Add to the ActorFilmList (list of ViewModel objects)
                ActorFilmList.Add(toAdd);
            }
            // Send the ActorFilmListViewModel List to the View for dispaly
            return View(ActorFilmList);
        }

        // GET: ActorFilms/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActorFilm actorFilm = db.ActorFilms.Find(id);
            if (actorFilm == null)
            {
                return HttpNotFound();
            }
            return View(actorFilm);
        }

        // GET: ActorFilms/Create
        // Define default values for the class
        public ActionResult Create(int FilmId = 0, int ActorId = 0)
        {
            // =================================================================
            // FILMS
            // =================================================================
            // From the Films model DbSet
            // select all columns from the database
            // order by the film name
            // LINQ Query
            var filmQuery =
                // Select a model/record type from the DB, use m as keyword
                from m in db.Films
                    // Alphabetical ordering
                orderby m.FilmName
                // Selects which column/fields to return (m is whole model)
                select m;

            // If no id set:
            //      Construct full films dropdown list without preselection
            //      Do so from the query results and display the FIlmname
            //      Store in FilmId in the ViewBag
            // Else
            //      Construct the same way, but with FilmId preselected
            if (FilmId == 0)
            {
                // Collection to send values for the View to access
                ViewBag.FilmId =
                    // Assign a dropdown list value, generated from filmQuery
                    // with value FilmId and name FilmName
                    // null determines no preselection
                                        new SelectList(filmQuery, "FilmId", "FilmName", null);
            }
            else
            {
                ViewBag.FilmId =
                    // FilmId determines preselection: if it was sent to this
                    // method, then we know we should interpret it and display
                    // that
                                        new SelectList(filmQuery, "FilmId", "FilmName", FilmId);
            }

            // =================================================================
            // ACTORS
            // =================================================================
            // From the Actors model DbSet
            // select the fname and sname as a new field called namespace Name
            // and order the Actorid by the sname
            // LINQ Query
            var actorsQuery =
                from a in db.Actors
                orderby a.ActorSname
                // Create new variable called Name; concatenate it as:
                // Actor First Name + *space* + Actor Surname
                // and select the ID after the comma
                select new
                {
                    Name = a.ActorFname + " " + a.ActorSname,
                    a.ActorId
                };

            // If no id set:
            //      Construct full films dropdown list without preselection
            //      Do so from the query results and display the concactenated Name
            //      Store in FilmId in the ViewBag
            // Else
            //      Construct the same way, but with ActorId preselected
            if (ActorId == 0)
            {
                ViewBag.ActorId =
                    new SelectList(actorsQuery, "ActorId", "Name", null);
            }
            else
            {
                ViewBag.ActorId =
                    new SelectList(actorsQuery, "ActorId", "Name", ActorId);
            }

            // Generate the view
            return View();
        }

        // POST: ActorFilms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ActorFilmId,ActorId,FilmId")] ActorFilm actorFilm)
        {
            if (ModelState.IsValid)
            {
                db.ActorFilms.Add(actorFilm);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(actorFilm);
        }

        // GET: ActorFilms/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActorFilm actorFilm = db.ActorFilms.Find(id);
            if (actorFilm == null)
            {
                return HttpNotFound();
            }

            // =================================================================
            // FILMS
            // =================================================================
            // From the Films model DbSet
            // select all columns from the database
            // order by the film name
            // LINQ Query
            var filmQuery =
                from m in db.Films
                orderby m.FilmName
                select m;
            // Construct full films dropdown list, preselected with foreign key
            // Do so from the query results and display the FIlmname
            // Store in FilmId in the ViewBag
                        ViewBag.Filmid = new SelectList(filmQuery, "FilmId", "FilmName", actorFilm.FilmId);

            // =================================================================
            // ACTORS
            // =================================================================
            // From the Actors model DbSet
            // select the fname and sname as a new field called namespace Name
            // and order the Actorid by the sname
            // LINQ Query
            var actorQuery =
                from a in db.Actors
                orderby a.ActorSname
                select new
                {
                    Name = a.ActorFname + " " + a.ActorSname,
                    a.ActorId
                };

            // Construct full films dropdown list, preselected with foreign key
            // Do so from the query results and display the concactenated Name
            // Store in FilmId in the ViewBag
            ViewBag.Actorid = new SelectList(actorQuery, "ActorId", "Name", actorFilm.ActorId);

            return View(actorFilm);
        }

        // POST: ActorFilms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ActorFilmId,ActorId,FilmId")] ActorFilm actorFilm)
        {
            if (ModelState.IsValid)
            {
                db.Entry(actorFilm).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(actorFilm);
        }

        // GET: ActorFilms/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActorFilm actorFilm = db.ActorFilms.Find(id);
            if (actorFilm == null)
            {
                return HttpNotFound();
            }
            return View(actorFilm);
        }

        // POST: ActorFilms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ActorFilm actorFilm = db.ActorFilms.Find(id);
            db.ActorFilms.Remove(actorFilm);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}