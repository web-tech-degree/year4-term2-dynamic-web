﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AiReviews.AiriCode;
using AiReviews.Models;
using AiReviews.Models.ViewModels;
using Microsoft.Ajax.Utilities;
using PagedList;

namespace AiReviews.Controllers
{
    public class ActorsController : Controller
    {
        private DBContext db = new DBContext();

        // JSON/AJAX Autocomplete on search using autocomplete jQuery UI plugin
        public ActionResult Search(string searchTerm)
        {
            // Based on where we want to search
            // Select all the names in the db
            // only get the ID and name
            // pass them into variables
            var searchables =
                from n in db.Actors
                select new
                {
                    id = n.ActorId,

                    name = n.ActorFname,
                    extra1 = n.ActorSname
                };

            // Now check if the searchTerm matches anything in the names var
            searchables = searchables.Where(n =>
               n.name.Contains(searchTerm) ||
               n.extra1.Contains(searchTerm));

            // Convert to JSON; return it
            return Json(searchables, JsonRequestBehavior.AllowGet);
        }

        // GET: Actors
        public ActionResult Index(int? page, string lastSort, string clickedSort, string currentName, string searchName, string currentGenre, string searchGenre)
        {
            // For the ViewBag to keep a note of current sort order
            if (!String.IsNullOrEmpty(lastSort))
            {
                ViewBag.LastSort = lastSort;
            }
            // Make defualt be firstName
            else
            {
                ViewBag.LastSort = "firstName";
            }

            // If clickedSort contains something, aka an option was clicked
            if (!String.IsNullOrEmpty(clickedSort))
            {
                // If the current sort "contains" the clicked sort, aka clicked current option (regardless of if it's inverted)
                if (ViewBag.LastSort.Contains(clickedSort))
                {
                    // If it was already "inverted", trim it to not identify as inverted
                    if (ViewBag.LastSort.Contains("Inverted"))
                    {
                        ViewBag.LastSort = clickedSort;
                    }
                    // Otherwise, suffix "inverted"
                    else
                    {
                        ViewBag.LastSort = lastSort + "Inverted";
                    }
                }
                // Since the current sort didn't "contain" the clicked sort, just make it normally sorted via that type
                else
                {
                    ViewBag.LastSort = clickedSort;
                }
            }

            // Reset to page 1 if user searched, otherwise push the current params into what will be checked next
            if (searchName != null)
                page = 1;
            else
                searchName = currentName;

            // Update ViewBag
            ViewBag.SearchName = searchName;

            // Select all the actors in the db
            var actors = from f in db.Actors select f;

            // Check if the search string is not empty
            // Get only the actors that match the inputted name
            // Concatenate the firstname and surname so the user can search for both together
            if (!String.IsNullOrEmpty(searchName))
            {
                actors = actors.Where(z => (z.ActorFname + " " + z.ActorSname).Contains(searchName));
            }

            // Go over what the new sort should be and apply it
            switch (ViewBag.lastSort)
            {
                case "firstName":
                    actors = actors.OrderBy(z => z.ActorFname);
                    break;
                case "firstNameInverted":
                    actors = actors.OrderByDescending(z => z.ActorFname);
                    break;
                case "lastName":
                    actors = actors.OrderBy(z => z.ActorSname);
                    break;
                case "lastNameInverted":
                    actors = actors.OrderByDescending(z => z.ActorSname);
                    break;
                case "birthDate":
                    actors = actors.OrderBy(z => z.ActorDateOfBirth);
                    break;
                case "birthDateInverted":
                    actors = actors.OrderByDescending(z => z.ActorDateOfBirth);
                    break;
                default:
                    actors = actors.OrderBy(z => z.ActorFname);
                    break;
            }

            // How many records per page (could also be a param...)
            int pageSize = 3;

            // If page is null: set to 1
            // otherwise: keep page value
            int pageNumber = (page ?? 1);

            // Send the updated films list to the View: Use PagedList to implement pagination,
            // and proide the pageSize
            return View(actors.ToPagedList(pageNumber, pageSize));
        }

        // GET: Actors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor activeActor = db.Actors.Find(id);
            if (activeActor == null)
            {
                return HttpNotFound();
            }

            //  Create a list for the view model to link Film and Actor
            List<ActorFilmListViewModel> ActorFilmList =
                new List<ActorFilmListViewModel>();

            // Separate list for the ActorFilm credits to get the keys
            List<ActorFilm> actorFilmCredits;

            // Populate the ActorFilms list by selecting all records
            // from the db context
            actorFilmCredits = db.ActorFilms.ToList();

            // Keep track of whether or not at least one actor film will be spat out
            bool atLeastOneResultFound = false;

            

            // Loop through each record to get the foreign keys
            // then populate the view model with the relevant Film/Actor
            foreach (ActorFilm i in actorFilmCredits)
            {
                // Only run on the current actor
                if (i.ActorId == id)
                {
                    atLeastOneResultFound = true;

                    // Match the ID between ActorFilm and Film, store the result in film
                    Film film = db.Films.Where(z => z.FilmId == i.FilmId).Single();

                    // Match the ID between ActorFilm and Film, store the result in actor
                    Actor actor = db.Actors.Where(z => z.ActorId == i.ActorId).Single();

                    // New ActorFilmListViewModel object to then add to the list
                    ActorFilmListViewModel toAdd = new ActorFilmListViewModel();

                    // Get the ActorFilmCredit, Film and Actor records.
                    // use these to prepare what should be added
                    toAdd.ActorFilmCredit = i;
                    toAdd.Film = film;
                    toAdd.Actor = actor;

                    // Add to the ActorFilmList (list of ViewModel objects)
                    ActorFilmList.Add(toAdd);
                }
            }
            // If at least one actor was found, ViewBag that it was lul I'm pretty sure this is not how I should do it
            if (atLeastOneResultFound)
            {
                ViewBag.ActorStarredInAtLeastOneMovie = true;
            }
            // If no actor films found, add the actor from the actor db into the viewmodel
            else
            {
                ViewBag.ActorStarredInAtLeastOneMovie = false;
                // New ActorFilmListViewModel object to then add to the list
                ActorFilmListViewModel toAdd = new ActorFilmListViewModel();
                toAdd.Actor = activeActor;
                ActorFilmList.Add(toAdd);
            }
            // Send the ActorFilmListViewModel List to the View for dispaly
            return View(ActorFilmList);
            // return View(activeActor);
        }

        // GET: Actors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Actors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        // Bind all the fields from the model to a Actor instance called actor
        // Get the uploaded (posted) file and associates it for handling using the name upload
        public ActionResult Create([Bind(Include = "ActorId,ActorFname,ActorSname," + "ActorDesc,ActorDateOfBirth,ActorImage")] Actor actor, HttpPostedFileBase upload)
        {
            // If the data in the form is valid
            if (ModelState.IsValid)
            {
                // Check if a file was uploaded
                if (upload != null && upload.ContentLength > 0)
                {
                    // Check MIME types against the valid types below
                    if (upload.ContentType == "image/jpeg" ||
                        upload.ContentType == "image/jpg" ||
                        upload.ContentType == "image/gif" ||
                        upload.ContentType == "image/png")
                    {
                        // Construct a path to put the file in an Images subfolder in Content
                        string path = Path.Combine(
                            Server.MapPath("~/Content/Images"),
                            Path.GetFileName(upload.FileName)
                        );

                        // Save the file to that path location
                        upload.SaveAs(path);

                        // Store the relative path to the image in the database
                        actor.ActorImage = "~/Content/Images/" + Path.GetFileName(upload.FileName);
                    }
                    else
                    {
                        // Construct a message that can be displayed in the view
                        ViewBag.Message = "Not valid image format";
                    }
                }
                // Add the actor to the database and save
                db.Actors.Add(actor);
                db.SaveChanges();
                // Redirect to Index
                return RedirectToAction("Index");
            }

            return View(actor);
        }

        // GET: Actors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor actor = db.Actors.Find(id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            return View(actor);
        }

        // POST: Actors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ActorId,ActorFname,ActorSname," + "ActorDesc,ActorDateOfBirth,ActorImage")] Actor actor, HttpPostedFileBase upload)
        {
            // If the data in the form is valid
            if (ModelState.IsValid)
            {
                // Check if a file was uploaded
                if (upload != null && upload.ContentLength > 0)
                {
                    // Check MIME types against the valid types below
                    if (upload.ContentType == "image/jpeg" ||
                        upload.ContentType == "image/jpg" ||
                        upload.ContentType == "image/gif" ||
                        upload.ContentType == "image/png")
                    {
                        // Construct a path to put the file in an Images subfolder in Content
                        string path = Path.Combine(
                            Server.MapPath("~/Content/Images"),
                            Path.GetFileName(upload.FileName)
                        );

                        // Save the file to that path location
                        upload.SaveAs(path);

                        // Store the relative path to the image in the database
                        actor.ActorImage = "~/Content/Images/" + Path.GetFileName(upload.FileName);
                    }
                    else
                    {
                        // Construct a message that can be displayed in the view
                        ViewBag.Message = "Not valid image format";
                    }
                }

                db.Entry(actor).State = EntityState.Modified;
                db.SaveChanges();
                // Redirect to Index
                return RedirectToAction("Index");
            }

            return View(actor);
        }

        // GET: Actors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor actor = db.Actors.Find(id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            return View(actor);
        }

        // POST: Actors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Actor actor = db.Actors.Find(id);
            db.Actors.Remove(actor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}